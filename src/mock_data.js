export const HAML_WITH_RUBY = `
- # HAML with Ruby

- form = local_assigns.fetch(:form)

= form.gitlab_ui_checkbox_component :only_allow_merge_if_pipeline_succeeds,
  s_('MergeChecks|Pipelines must succeed'),
  help_text: s_("MergeChecks|Merge requests can't be merged if the latest pipeline did not succeed or is still running.")
.gl-pl-6
  = form.gitlab_ui_checkbox_component :allow_merge_on_skipped_pipeline,
    s_('MergeChecks|Skipped pipelines are considered successful'),
    help_text: s_('MergeChecks|Introduces the risk of merging changes that do not pass the pipeline.'),
    checkbox_options: { class: 'gl-pl-6' }
= form.gitlab_ui_checkbox_component :only_allow_merge_if_all_discussions_are_resolved,
  s_('MergeChecks|All threads must be resolved'),
  checkbox_options: { data: { qa_selector: 'only_allow_merge_if_all_discussions_are_resolved_checkbox' } }
  `;

export const PLAIN_HAML = `
- # Pain HAML
  
!!! 5
%html
  %body
    %h1.foo-bar This works fine
    %span.hello.world syntax is fine here
    .broken{ broken: 'yes' }
    %anything.working{ working: 'fine' }
      .also-broken
      .i-am-broken
        .me-too
          %h1.good All good!
`;

export const JS = `
// JavaScript

function exampleFunction(value1, value2) {
    var result = value1 * value2;

    return result;
}

var first = prompt("Enter a number: ")
var second = prompt("Enter a second number: ")

console.log("The product of " + first + " and " + second + " is " + exampleFunction(first, second))
// or in a browser
document.write("The product of " + first + " and " + second + " is " + exampleFunction(first, second))

`;

export const HASKELL = `
-- Haskell

jsonStringContentP :: Parser String
jsonStringContentP = wrapped1P dP $ tseqP dP (charP '\\') passChar
  where
      dP = charP '"' -- some comment

jsonString :: Parser JsonValue
jsonString = JsonString <$> jsonStringContentP
jsonArray :: Parser JsonValue
`;
