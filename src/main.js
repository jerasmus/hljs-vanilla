import Vue from 'vue'
import App from './App.vue'

import './assets/main.css'
import 'highlight.js/styles/monokai.css'; // I'll test with Monokai as my base theme

new Vue({
  render: (h) => h(App)
}).$mount('#app')
